package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct implements MenuItem, Serializable {
    private String nume;
    public List<MenuItem> ingredients = new ArrayList<>();
    public int pret;
    

    public CompositeProduct(String _nume) {
        this.nume = _nume;
        this.pret = computePrice();
    }

    @Override
    public int computePrice() {
        pret = 0;

        for (MenuItem menuItem : ingredients) {
        	
            menuItem.setPrice(Restaurant.getItem(menuItem.getName()).computePrice());
            pret += menuItem.computePrice();
        }
        
        return pret;
    }

    
    @Override
    public void setName(String _nume) {
    	
        this.nume = _nume;
    }
    
    @Override
    public void setPrice(int _pret) {
    	
        this.pret = _pret;
    }
    
    @Override
    public String getName() {
    	
        return nume;
    }
    
    public void addIngredient(MenuItem ingredient) {
        ingredients.add(ingredient);
        this.pret = computePrice();
    }
    
    public List<MenuItem> getIngredients() {
    	
        return ingredients;
    }
   
}