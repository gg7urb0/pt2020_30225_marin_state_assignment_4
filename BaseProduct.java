package BusinessLayer;

import java.io.Serializable;

public class BaseProduct implements MenuItem, Serializable {
    public String nume;
    public int pret;

    public BaseProduct(String _nume, int _pret) {
        this.nume = _nume;
        this.pret = _pret;
    }

    @Override
    public int computePrice() {
        return this.pret;
    }

    @Override
    public String getName() {
        return this.nume;
    }

    @Override
    public void setPrice(int _pret) {
        this.pret = _pret;
    }

    @Override
    public void setName(String _nume) {
        this.nume = _nume;
    }
}