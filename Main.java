package Main;

import DataLayer.FileWriters;
import PresentationLayer.AdministratorGUI;
import PresentationLayer.WaiterGUI;

public class Main {
    
    public static WaiterGUI waiterGUI;

    public static void main(String[] args) {
        FileWriters.initializareBuffer();
        new AdministratorGUI();
        waiterGUI = new WaiterGUI();
    }
   
}