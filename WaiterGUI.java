package PresentationLayer;

import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import Main.Main;

import javax.swing.*;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WaiterGUI {
    private JTable jTable;
    private JFrame jFrame = new JFrame();
    Color culoarebackground=new Color(0x20ffffff);
    public WaiterGUI() {
 
        jFrame.setLayout(null);
        jFrame.setSize(400, 650);
        jFrame.setTitle("Waiter");
        jFrame.setLocationRelativeTo(null);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton addButton = new JButton("Comanda Noua");
        addButton.setBounds(5, 5, 376, 25);
        addButton.setBackground(culoarebackground);
        jFrame.add(addButton);
        JButton billButton = new JButton("Nota de plata");
        billButton.setBounds(5, 35, 376, 25);
        billButton.setBackground(culoarebackground);
        jFrame.add(billButton);
        jFrame.add(createJScrollPane());
       
        addButtonActionListener(addButton, jFrame);
        billButtonActionListener(billButton, jFrame);

        jFrame.setVisible(true);
    }

    private JScrollPane createJScrollPane() {
        jTable = Restaurant.createOrdersTable();
        jTable.getTableHeader().setReorderingAllowed(false);
        jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        if (jTable.getRowCount() > 0)
            jTable.setRowSelectionInterval(0, 0);
        JScrollPane jScrollPane = new JScrollPane(jTable);
        jScrollPane.setBounds(5, 65, 376, 544);
        return jScrollPane;
    }

    private void addButtonActionListener(JButton addButton, JFrame jFrame) {
        addButton.addActionListener(e -> {
            WaiterAddNewOrderFrame addOrderFrame = new WaiterAddNewOrderFrame();
            addOrderFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    Main.waiterGUI = new WaiterGUI();
                    jFrame.dispose();
                }
            });
        });
    }
    private void billButtonActionListener(JButton billButton, JFrame jFrame) {
        billButton.addActionListener(e -> {
            Order order = Restaurant.getOrder(Integer.parseInt(jTable.getValueAt(jTable.getSelectedRow(), 0).toString()));
            JOptionPane.showMessageDialog(null, Restaurant.generareNota(order));
            Restaurant.deleteOrder(order);
            Main.waiterGUI = new WaiterGUI();
            jFrame.dispose();
        });
    }
    JFrame getJFrame() {
        return jFrame;
    }
}