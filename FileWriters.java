package DataLayer;


import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

import java.io.*;
import java.util.List;

public class FileWriters {
    private static BufferedWriter buffWriter;

    public static void initializareBuffer() {
        try {
            buffWriter = new BufferedWriter(new FileWriter("bills.txt"));
        } catch (IOException e) {
            System.out.println(e.toString());
        }

    }
    
    public static String generateBill(Order order) {
        try {
            String afis = "\nORDER " + order.idComanda + ":\nTable: " + order.masa + "   -   " + order.data + "\n";
            List<MenuItem> menuItems = order.getListOfItems();
            int total = 0;
            for (int i = 0; i < menuItems.size(); i++) {
                int quantity = 1;
                while (i != menuItems.size() - 1 && menuItems.get(i).getName().compareTo(menuItems.get(i + 1).getName()) == 0) {
                    quantity++;
                    i++;
                }
                MenuItem menuItem = Restaurant.getItem(menuItems.get(i).getName());
                afis = afis.concat(
                        menuItem.getName() + " - " + menuItem.computePrice() + " RON x" + quantity + "\n");
                total += menuItem.computePrice() * quantity;
            }
            afis = afis.concat("\nTOTAL: " + total + " RON\nVa multumim!\n");
            buffWriter.append(afis);
            return afis;
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return null;
    }


    
}