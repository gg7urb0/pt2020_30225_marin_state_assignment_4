package BusinessLayer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Order implements Comparable<Order> {
	
    public static int id = 0;
    public int idComanda, masa;
    public boolean pregatit;
    public LocalDate data;
    public Collection<MenuItem> produse;


    public Order(int _masa, List<MenuItem> _menuItems) {
    	
        this.idComanda = id;
        id++;
        this.data = LocalDate.now();
        this.masa = _masa;
        this.produse = _menuItems;
        this.pregatit = false;
    }
 
    @Override
    public int compareTo(Order o) {
    	
        Integer id1 = this.idComanda;
        Integer id2 = o.idComanda;
        return id1.compareTo(id2);
    }   
    
    public List<MenuItem> getListOfItems() {
    	
        return new ArrayList<>(produse);
    }
    
}