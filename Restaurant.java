package BusinessLayer;

import DataLayer.FileWriters;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class Restaurant implements RestaurantProcessing {
	
    public static Map<Order, Collection<MenuItem>> comenzi = new HashMap<>();
    public static List<MenuItem> produse = new ArrayList<>();

    public static void creareProdus(MenuItem produs) {
    	
        assert produs != null;
        assert produse != null;
        produse.add(produs);
        assert produse.size() > 0;
    }

    public static void stergereProdus(MenuItem produs) {
        assert produs != null;
        assert produse != null;
        produse.remove(getItem(produs.getName()));
    }

    public static Order getOrder(int orderID) {
        List<Order> orderList = creareListaComenzi();
        for (Order order : orderList) {
            if (order.idComanda == orderID)
                return order;
        }
        return null;
    }
    
    public static void creareComanda(Order comanda) {
        assert comanda != null;
        comenzi.put(comanda, comanda.produse);
        assert comenzi.size() > 0;
    }

    
    public static JTable createOrdersTable() {
        List<Order> orderList = creareListaComenzi();
        JTable table;
        String[] columnNames = {"Id", "Masa", "Data"};
        Object[][] comenzi = new Object[orderList.size()][3];

        for (int i = 0; i < orderList.size(); ++i) {
            comenzi[i][0] = orderList.get(i).idComanda;
            comenzi[i][1] = orderList.get(i).masa;
            comenzi[i][2] = orderList.get(i).data;
        }

        table = new JTable(comenzi, columnNames);
        table.setDefaultEditor(Object.class, null);
        return table;
    }

    public static void deleteOrder(Order order) {
        comenzi.remove(getOrder(order.idComanda));
    }

    public static JTable createTable() {
        JTable table;

        String[] columnNames = {"Nume", "Produs", "Tip"};
        Object[][] prod = new Object[produse.size()][3];

        for (int i = 0; i < produse.size(); i++) {
            prod[i][0] = produse.get(i).getName();
            prod[i][1] = produse.get(i).computePrice();
            prod[i][2] = produse.get(i).getClass().getSimpleName();
        }

        table = new JTable(prod, columnNames);
        table.setDefaultEditor(Object.class, null);

        return table;
    }

    public static MenuItem getItem(String name) {
        for (MenuItem menuItem : produse) {
            if (menuItem.getName().compareTo(name) == 0)
                return menuItem;
        }

        return null;
    }

    

    public static List<MenuItem> getBaseMenuItems() {
        List<MenuItem> toReturn = new ArrayList<>();

        for (MenuItem menuItem : produse) {
            if (menuItem.getClass().getSimpleName().compareTo("BaseProduct") == 0)
                toReturn.add(menuItem);
        }
        return toReturn;
    }


    public static void editareProdus(MenuItem produs, String nume_nou, int pret_nou) {
        assert produs != null;
        assert pret_nou >= 0;
        int oldSize = produse.size();

        for (MenuItem menuItem1 : produse) {
            if (menuItem1.getName().compareTo(produs.getName()) == 0) {
                menuItem1.setName(nume_nou);
                menuItem1.setPrice(pret_nou);
                break;
            }
        }

        assert oldSize == produse.size();
    }

    public static List<Order> creareListaComenzi() {
        Set<Order> orderSet = comenzi.keySet();
        List<Order> orderList = new ArrayList<>(orderSet);
        Collections.sort(orderList);

        return orderList;
    }
    
    public static void updateazaProduse() {
        for (MenuItem menuItem : produse) {
            menuItem.computePrice();
        }
    }

    public static boolean poateFiSters(String nume) {
        if (getItem(nume).getClass().getSimpleName().compareTo("CompositeProduct") == 0)
            return true;

        for (MenuItem menuItem : produse) {
            if (menuItem.getClass().getSimpleName().compareTo("CompositeProduct") == 0) {
                CompositeProduct compositeProduct = (CompositeProduct) menuItem;

                List<MenuItem> ingredients = compositeProduct.getIngredients();
                for (MenuItem menuItem1 : ingredients) {
                    if (nume.compareTo(menuItem1.getName()) == 0)
                        return false;
                }
            }
        }

        List<Order> orderList = creareListaComenzi();
        for(Order comanda : orderList) {
            for(MenuItem produs : comanda.getListOfItems()) {
                if(produs.getName().compareTo(nume) == 0)
                    return false;
            }
        }
        return true;
    }
    public static String generareNota(Order comanda) {
        assert comanda != null;
        return FileWriters.generateBill(comanda);
    }
}